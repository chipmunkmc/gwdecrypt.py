#!/usr/bin/env python3
# W.J. van der Laan 2017, distributed under MIT license
# James Hilliard 2017
import binascii
import base64
import struct
import json
import os, sys
import hashlib
from Crypto import Random
from Crypto.Cipher import AES

KEY = binascii.a2b_hex(b'fffffbffeffffbfffbbfffbfdbfff7ffffffffffffffdfffff7fffffbfffffff')

def unpad(s):
    '''PKCS7 unpad.'''
    padlen = s[len(s)-1]
    if padlen > 16:
        raise ValueError('Invalid padding')
    return s[:-padlen]

def decrypt(data_in):
    # first 32 bytes are IV, we only need 16 of that
    iv = data_in[0:16]
    cipher = AES.new(KEY, AES.MODE_CBC, iv)
    data_out = cipher.decrypt(data_in[32:])
    data_out = unpad(data_out)

    crc = data_out[0:4]
    ccrc = struct.pack('I',binascii.crc32(data_out[4:]))
    assert(crc == ccrc)
    json_data = json.loads(data_out[4:].decode())
    return json_data

def main():
    if len(sys.argv) < 2:
        print('Usage: %s /path/to/configfile.bin' % os.path.basename(sys.argv[0]))
        exit(1)
    with open(sys.argv[1], 'rb') as f:
        data_in = base64.b64decode(f.read())

    # decode second layer, create one huge JSON file with record types at top level
    json_data = decrypt(data_in)
    out_recs = {}
    for record in json_data:
        
        d = base64.b64decode(record['data'])
        assert(record['type'] not in out_recs) # duplicate
        out_recs[record['type']] = decrypt(d)

    if len(sys.argv) == 3:
        with open(sys.argv[2], 'w') as outfile:
            json.dump(out_recs, outfile, indent=4, separators=(',', ': '))
    else:
        json.dump(out_recs, sys.stdout, indent=4, separators=(',', ': '))

if __name__ == '__main__':
    main()